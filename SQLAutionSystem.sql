create database AuctionSystem
go
use AuctionSystem
go

-- Thông tin nhân viên
create table Staff(
	StaffId int not null primary key, -- Mã nhân viên
	UserName char(50) not null, -- Tên đăng nhập, cũng là tên tài khoản
	PasswordStaff char(16) not null, -- Mật khẩu
	Email char(50) not null, -- Email nhân viên
	FullName nvarchar(70) not null, -- Tên thật
	Birthday date not null, -- Ngày tháng năm sinh
	JoinDay date not null, -- Ngày tham gia vào hệ thống
	StaffAddress nvarchar(100) not null, -- Địa chỉ
);

-- Thông tin người dùng
create table UserAccount(
	UserId int not null primary key, -- Mã số người dùng, số tự nhiên tăng dần
	UserName char(50) not null, -- Tên đăng nhập, cũng là tên tài khoản
	PasswordAccount  char(16) not null, -- Mật khẩu tài khoản
	Email char(50) not null, --  Email người dùng
	FullName nvarchar(70) not null, -- Tên thật
	Birthday date not null, -- Ngày tháng năm sinh
	JoinDay date not null, -- Ngày tham gia vào hệ thống
	UserAddress nvarchar(100) not null, -- Địa chỉ
	Reliability float not null, -- Mức độ tin cậy (khi bán hàng, mặc định ban đầu là 0)
	LevelAccount int not null, -- Cấp của người dùng 
							   --(dùng cho việc thiết lập các tính năng nâng cao và khoá tài khoản)
							   -- Bình thường tài khoản là 0
							   -- Khi được nâng cấp là 1 (hoặc 2, 3,...)
							   -- Khi bị khoá là -1
	StaffId int not null, -- Mã nhân viên tạo tài khoản, khoá ngoại đến bảng Staff
	foreign key (StaffId) references Staff(StaffId)
);

-- Vì mỗi sản phẩm khi được đăng lên hệ thống sẽ được tạo ra một phiên đấu giá
-- Nên tích hợp thông tin sản phẩn và phiên đấu giá của sản phẩm đó làm một
create table ProductAuction(
	ProductAuctionId int not null primary key, -- Mã số sản phẩm
	StartPrice float not null, -- Giá khởi điểm
	BuynowPrice float, -- Giá mua ngay
	Situation char(5) not null,-- Trạng thái sản phẩm 
	check (Situation in ('New', 'Used')), -- Mới(new) hoặc đã qua sử dụng (Used)
	ImgPath char(200) not null, -- Đường dẫn đến thư mục chứa các hình ảnh sản phầm trong hệ thống
	Seller int not null, -- Mã số người bán, khoá ngoại đến UserAccount
	StartTime date not null, -- Thời gian bắt đầu
	EndTime date not null, -- Thời gian kết thúc
	BuyItNow bit not null, -- true: sản phẩm đấu giá được một người "mua ngay"
	Buyer int, -- Mã người mua, nếu chưa có ai mua sẽ null
	BuyPrice float, -- Giá bán cuối cùng của sản phẩm
	Increment float not null, -- Mức giá tăng sau mỗi lần ra giá
	StaffId int not null, -- Nhân viên duyệt sản phẩm, khoá ngoại đến bảng Staff
	foreign key (StaffId) references Staff(StaffId),
	foreign key (Seller) references UserAccount(UserId)
 );
-- Ngôn ngữ hệ thống hỗ trợ
-- Ví dụ ('en', 'English'), ('zh', '中文'), ('vi', 'Tiếng Việt'), .... 
create table Languages(
	LanguageCode char(2) not null primary key, -- Mã ngôn ngữ
	LanguageName nvarchar(20) not null, -- Tên ngôn ngữ
);

-- Danh mục sản phẩm, 
-- ví dụ: (1, 'en', 'Fashion'), (1, 'vi', 'Thời trang'), (1, 'zh', '時裝')
--        (2, 'vi', 'Electronics'), (2, 'vi', 'Đồ điện tử'), (2, 'zh', '電子產品')
create table Category(
	CategoryId int not null, -- Mã danh mục, số tự nhiên tăng dần
	LanguageCode char(2) not null, -- Ngôn ngữ thể hiện (trên dòng dữ liệu đó)
	CategoryName nvarchar(50) not null, -- Tên danh mục sản phẩm (thể hiện bằng nhiều ngôn ngữ)
	primary key (CategoryId, LanguageCode),
	foreign key (LanguageCode) references Languages(LanguageCode)
);

-- Liên hệ giữa danh mục sản phẩm (Catogory) và sản phẩm đấu giá (ProductAuction)
-- Cho biết thông tin chi tiết của sản phẩm (theo các dnah mục và ngôn ngữ)
-- Bình thường mỗi sản phẩm chỉ thuộc về một danh mục, nhưng khi sử dụng tính năng nâng cao
-- người mua có thể đưa sản phẩm của mình vào nhiều danh mục
create table ProductDetail(
	ProductId int not null, -- Mã sản phẩm đấu giá, khoá ngoại đến ProductAuction
	CategoryId int not null, -- Mã danh mục sản phẩm, khoá ngoại đến ProductList
	LanguageCode char(2) not null, -- Mã ngôn ngữ hỗ trợ, khoá ngoại đến Languages
	ProductName nvarchar(100) not null, -- Tên sản phẩm (theo ngôn ngữ)
	Unit nvarchar(20) not null, -- Đơn vị tính cho sản phẩm (cái, cặp, bộ,...) (theo ngôn ngữ)
	ProductInfor nvarchar(1000) not null, -- Thông tin mô tả sản phẩm (theo ngôn ngữ)
	primary key (CategoryId, LanguageCode, ProductId),
	foreign key (ProductId) references ProductAuction(ProductAuctionId),
	foreign key (CategoryId, LanguageCode) references Category(CategoryId, LanguageCode)
)

-- Chi tiết cho phiên đấu giá
-- Lưu lại mỗi lần có người ra giá
create table AuctionDetail(
	ProductId int not null, -- Mã sản phẩm đấu giá, khoá ngoại đến ProductionAution
	Number int not null, -- STT lần ra giá của người mua trong phiên đấu
	Price float not null, -- Giá người mua đặt trong lần ra giá
	AuctionTime time not null, -- Thời gian ra giá
	primary key (ProductId, Number),
	foreign key (ProductId) references ProductAuction(ProductAuctionId)
);

-- Báo cáo vi phạm (phàn nàn) của người mua đối với sản phẩm
create table Report(
	ProductId int not null, -- sản phẩm bị báo cáo 
	Number int, -- STT lần bị gởi báo cáo (một sản phẩm có thể bị báo cáo nhiều lần)
				-- Bắt đầu từ 1
	Content nvarchar(200) not null, -- Nội dung báo cáo
	ReportDate date not null, -- ngày gởi báo cáo
	StaffId int not null, -- Khoá ngoại đến bảng Staff
	-- Mặc định là chỉ có người mua được sản phầm mới được gởi báo cáo
	-- Nên không cần lưu thông tin người gởi
	primary key (ProductId, Number),
	foreign key (ProductId) references ProductAuction(ProductAuctionId),
	foreign key (StaffId) references Staff(StaffId)
);

-- Languages
insert into Languages values('en', 'English')
insert into Languages values('vi', N'Tiếng Việt')
insert into Languages values('zh', N'中文')

-- Category
insert into Category values(1,'en', 'Fashion')
insert into Category values(1,'vi', N'Thời Trang')
insert into Category values(1,'zh', N'時裝')
insert into Category values(2,'en', 'Electronics')
insert into Category values(2,'vi', N'Thiết Bị Điện tử')
insert into Category values(2,'zh', N'電子產品')
insert into Category values(3,'en', 'Books')
insert into Category values(3,'vi', N'Sách')
insert into Category values(3,'zh', N'書本')
insert into Category values(4,'en', 'Sporting Goods')
insert into Category values(4,'vi', N'Dụng Cụ Thể Thao')
insert into Category values(4,'zh', N'體育用品')
insert into Category values(5,'en', 'Arts')
insert into Category values(5,'vi', N'Tác Phẩm Nghệ Thuật')
insert into Category values(5,'zh', N'藝術品')
insert into Category values(6,'en', 'Antiques')
insert into Category values(6,'vi', N'Đồ Cổ')
insert into Category values(6,'zh', N'古董')
insert into Category values(7,'en', 'Motor')
insert into Category values(7,'vi', N'Phương Tiện Giao Thông')
insert into Category values(7,'zh', N'運輸')
insert into Category values(8,'en', 'Mobile Phones')
insert into Category values(8,'vi', N'Điện Thoại di động')
insert into Category values(8,'zh', N'移動電話')
insert into Category values(9,'en', 'Cosmetics')
insert into Category values(9,'vi', N'Mỹ phẩm')
insert into Category values(9,'zh', N'化妝品')
insert into Category values(10,'en', 'Jewelry')
insert into Category values(10,'vi', N'Nữ Trang')
insert into Category values(10,'zh', N'首飾')
insert into Category values(11,'en', 'Musical Instrument')
insert into Category values(11,'vi', N'Nhạc Cụ')
insert into Category values(11,'zh', N'儀器')
insert into Category values(12,'en', 'Cameras')
insert into Category values(12,'vi', N'Máy Quay Phim, Máy Chụp Hình')
insert into Category values(13,'zh', N'照相機, 攝影機')
insert into Category values(13,'en', 'Home & Garden')
insert into Category values(13,'vi', N'Dụng Cụ Trong Nhà Và Sân Vườn')
insert into Category values(13,'zh', N'家居與園藝')

-- Staff
insert into Staff values(1,'staff1', '123', 'staff1@mail.com', N'Nguyễn An', '1/1/1980', '1/1/2010', N'Bình Thạnh, Tp Hồ Chí Minh')
insert into Staff values(2,'staff2', '123', 'staff2@mail.com', N'Trần Văn Bình', '1/3/1987', '1/1/2010', N'Bình Chánh, Tp Hồ Chí Minh')
insert into Staff values(3,'staff3', '123', 'staff3@mail.com', N'Hồ Thị Hoa', '1/10/1977', '1/1/2010', N'Dĩ An, Bình Dương')
insert into Staff values(4,'staff4', '123', 'staff4@mail.com', N'Nguyễn Minh', '1/30/1989', '1/1/2010', N'Quận 5, Tp Hồ Chí Minh')
insert into Staff values(5,'staff5', '123', 'staff5@mail.com', N'Bùi Văn Ninh', '12/9/1978', '1/1/2010', N'Cần Giộc, Long An')

-- UserAccount
insert into UserAccount values(1,'user1', 'abc', 'user1@mail.com', N'Trần C', '1/1/1980', '1/1/2010', N'Bình Thạnh, Tp Hồ Chí Minh', 0, 0, 1)
insert into UserAccount values(2,'user2', 'abc', 'user2@mail.com', N'Lý A', '1/1/1980', '1/1/2010', N'Bình Thạnh, Tp Hồ Chí Minh', 0, 0, 2)
insert into UserAccount values(3,'user3', 'abc', 'user3@mail.com', N'Nguyễn B', '1/1/1980', '1/1/2010', N'Bình Thạnh, Tp Hồ Chí Minh', 0, 0, 3)
insert into UserAccount values(4,'user4', 'abc', 'user4@mail.com', N'Nguyễn K', '1/1/1980', '1/1/2010', N'Bình Thạnh, Tp Hồ Chí Minh', 0, 0, 1)
insert into UserAccount values(5,'user5', 'abc', 'user5@mail.com', N'Phan D', '1/1/1980', '1/1/2010', N'Bình Thạnh, Tp Hồ Chí Minh', 0, 0, 2)

-- ProductAuction
insert into ProductAuction values(1, 50, 100, 'New',  'Content/ProductImg/Product1.jpg', 1, '11/20/2014 12:00:00 AM', '11/25/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(2, 50, 100, 'New',  'Content/ProductImg/Product2.jpg', 3, '11/21/2014 12:00:00 AM', '11/28/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(3, 50, 100, 'New',  'Content/ProductImg/Product3.jpg', 1, '11/23/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(4, 50, 100, 'New',  'Content/ProductImg/Product4.jpg', 2, '11/25/2014 12:00:00 AM', '11/20/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(5, 100, 500, 'New',  'Content/ProductImg/Product5.jpg', 3, '11/28/2014 12:00:00 AM', '12/2/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(6, 20, 200, 'New',  'Content/ProductImg/Product6.jpg', 4, '11/22/2014 12:00:00 AM', '11/30/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(7, 180, 500, 'Used',  'Content/ProductImg/Product7.jpg', 1, '11/23/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(8, 70, 380, 'New',  'Content/ProductImg/Product8.jpg', 4, '11/24/2014 12:00:00 AM', '11/28/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(9, 100, 700, 'New',  'Content/ProductImg/Product9.jpg', 5, '11/25/2014 12:00:00 AM', '11/28/2014 12:00:00 AM', 'false', null, null, 5, 1)
insert into ProductAuction values(10, 150, 800, 'New',  'Content/ProductImg/Product10.jpg', 5, '11/24/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 10, 1)
insert into ProductAuction values(11, 500, 2000, 'New',  'Content/ProductImg/Product11.jpg', 5, '11/24/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 10, 1)
insert into ProductAuction values(12, 450, 1700, 'Used',  'Content/ProductImg/Product12.jpg', 1, '11/24/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 2, 1)
insert into ProductAuction values(13, 200, 1200, 'Used',  'Content/ProductImg/Product13.jpg', 1, '11/22/2014 12:00:00 AM', '11/27/2014 12:00:00 AM', 'false', null, null, 10, 1)
insert into ProductAuction values(14, 100, 900, 'Used',  'Content/ProductImg/Product14.jpg', 5, '11/23/2014 12:00:00 AM', '12/1/2014 12:00:00 AM', 'false', null, null, 5, 2)
insert into ProductAuction values(15, 150, 2000, 'Used',  'Content/ProductImg/Product15.jpg', 4, '11/27/2014 12:00:00 AM', '12/3/2014 12:00:00 AM', 'false', null, null, 5, 2)
insert into ProductAuction values(16, 200, 900, 'Used',  'Content/ProductImg/Product16.jpg', 2, '11/20/2014 12:00:00 AM', '11/26/2014 12:00:00 AM', 'false', null, null, 10, 2)
insert into ProductAuction values(17, 300, 1500, 'New',  'Content/ProductImg/Product17.jpg', 3, '11/20/2014 12:00:00 AM', '11/25/2014 12:00:00 AM', 'false', null, null, 10, 2)
insert into ProductAuction values(18, 250, 8000, 'New',  'Content/ProductImg/Product18.jpg', 2, '11/25/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 5, 2)
insert into ProductAuction values(19, 400, 1500, 'New',  'Content/ProductImg/Product19.jpg', 3, '11/24/2014 12:00:00 AM', '11/30/2014 12:00:00 AM', 'false', null, null, 15, 2)
insert into ProductAuction values(20, 300, 900, 'New',  'Content/ProductImg/Product20.jpg', 1, '11/28/2014 12:00:00 AM', '12/1/2014 12:00:00 AM', 'false', null, null, 10, 2)
insert into ProductAuction values(21, 200, 800, 'New',  'Content/ProductImg/Product21.jpg', 2, '11/20/2014 12:00:00 AM', '11/25/2014 12:00:00 AM', 'false', null, null, 5, 2)
insert into ProductAuction values(22, 270, 1100, 'New',  'Content/ProductImg/Product22.jpg', 2, '11/20/2014 12:00:00 AM', '11/25/2014 12:00:00 AM', 'false', null, null, 5, 3)
insert into ProductAuction values(23, 250, 1000, 'Used',  'Content/ProductImg/Product23.jpg', 2, '11/25/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 5, 3)
insert into ProductAuction values(24, 500, 2200, 'Used',  'Content/ProductImg/Product24.jpg', 1, '11/24/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 15, 3)
insert into ProductAuction values(25, 300, 1200, 'New',  'Content/ProductImg/Product25.jpg', 4, '11/25/2014 12:00:00 AM', '11/29/2014 12:00:00 AM', 'false', null, null, 5, 3)

-- ProductDetail
insert into ProductDetail values(1, 1, 'en', 'Shirt Seagull', 'unit', 'Hollister by abercrombie men wipeout beach polo pique shirt seagull')
insert into ProductDetail values(2, 1, 'en', 'Men Shoes', 'pair', 'Kenneth cole mens way to go black or brown cognac slip-on driving drivers shoes')
insert into ProductDetail values(3, 1, 'en', 'Small Tote Bag', 'unit', 'Michael kors mandarin jet set travel small tote bag')
insert into ProductDetail values(4, 1, 'en', 'Men Watch', 'unit', 'New citizen eco-drive chronograph promaster men watch BJ2115-07E')
insert into ProductDetail values(5, 1, 'en', 'Jeans Trousers', 'pair', 'Tommy hilfiger denim jeans rebel dlim mens straight leg dark dtone blue new V177')
insert into ProductDetail values(6, 2, 'en', 'Apple iPad', 'unit','Apple iPad air 16GB tablet w/ retina display wifi, facetime & camera - 1st gen')
insert into ProductDetail values(7, 2, 'en', 'Headphones', 'unit','Sony MDR-1RNC brand NEW digital noise canceling headphones W/Inline remote & mic')
insert into ProductDetail values(8, 2, 'en', 'Paperwhite', 'unit','BRAND NEW amazon kindle paperwhite high eesolution WI-FI 4GB latest 2014 model')
insert into ProductDetail values(9, 2, 'en', 'Samsung Galaxy Tab', 'unit','Samsung galaxy tab 3 7 LCD screen 8GB wifi gold androif tablet')
insert into ProductDetail values(10, 2, 'en', 'WiFi Router', 'unit', 'TP-LINK TL-MR3020 portable 3G 4G USB modem internet share wireless N WiFi router')

insert into ProductDetail values(11, 1, 'vi', N'Áo Nữ', 'unit', N'Áo hoa đỏ hia túi thời trang')
insert into ProductDetail values(12, 1, 'vi', N'Đầm Nữ', 'unit', N'Đầm ngắn nữ đen')
insert into ProductDetail values(13, 1, 'vi', N'Đầm Nữ', 'unit', N'Đầm bodytrắng kem')
insert into ProductDetail values(14, 1, 'vi', N'Túi Vải', 'unit', N'Túi vải nam quai chéo')
insert into ProductDetail values(15, 1, 'vi', N'Áo Thun Nam', 'unit', N'Áo thun nam Polo Xanh lá thông cổ Sơ mi')
insert into ProductDetail values(16, 2, 'vi', N'IPhone 6 Phus', 'unit', N'Vi xử lý A8 giờ đây mạnh mẽ hơn, đáp ứng cho một màn hình hiển thị lớn hơn. Chip M8 làm nhiệm vụ tổng hợp dữ liệu của các cảm biến tiên tiến và điều khiển chúng.')
insert into ProductDetail values(17, 2, 'vi', N'Tủ Lạnh Electrolux', 'unit', 'FlexStor.Chuông báo cửa mở. Hệ thống khử mùi Deo Fresh. Ngăn rau củ Market Fresh')
insert into ProductDetail values(18, 2, 'vi', N'Tivi', 'unit', 'TIVI LCD LED LG 39LB561T.ATV')
insert into ProductDetail values(19, 2, 'vi', N'Máy Lạnh', 'unit', 'Máy lạnh LG S09ENA')
insert into ProductDetail values(20, 2, 'vi', N'Galaxy Samsung Note 4', 'unit', 'ĐTDĐ SAMSUNG GALAXY NOTE 4 SM-N910C TRẮNG')

insert into ProductDetail values(21, 2, 'zh', N'耳機', 'unit', N'可調過入耳式耳塞耳機3.5毫米的iPod MP3 MP4PC iPhone音樂')
insert into ProductDetail values(22, 2, 'zh', N'收音機', 'unit', N'金正 4.3吋 視頻 彩屏 擴音器 mp3 mp4 mp5多媒體播放機 看戲機 高清唱戲機 收音機 音箱')
insert into ProductDetail values(23, 2, 'zh', N'笔记本 ', 'unit', N'原装正品 iphone 苹果 贴膜 ipad 平板电脑 笔记本 贴膜')
insert into ProductDetail values(24, 2, 'zh', N'手提電腦 底板', 'unit', N'富士通 FUJITSU FMV-820NA 手提電腦 底板')
insert into ProductDetail values(25, 2, 'zh', N'平板', 'unit', N'粉红平板我第一年的孩子垫 TAB键益智玩具')