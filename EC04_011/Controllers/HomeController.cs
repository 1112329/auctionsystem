﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using EC04_011.Models;

namespace EC04_011.Controllers
{
    public class HomeController : MyBaseController
    {
        AuctionSystemEntities data = new AuctionSystemEntities();

        public ActionResult Index(string lang = "en")
        {
            var category = (from c in data.Categories where c.LanguageCode == lang select c).ToList();
            return View(category);
        }

        public ActionResult Search(string language, int CategoryId)//, int? page)
        {
            ViewBag.language = language;
            ViewBag.CategoryId = CategoryId;
            var product = (from pr in data.ProductDetails where pr.CategoryId == CategoryId && pr.LanguageCode == language select pr).ToList();//.ToPagedList(page ?? 1, 3);
            return PartialView("ResultSearch", product);
        }

        public ActionResult ChangeLanguage(string lang) 
        {
            new SiteLanguages().SetLanguage(lang);
            return RedirectToAction("Index", "Home", new { lang = lang });
        }
    }
}
