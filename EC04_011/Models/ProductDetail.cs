//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EC04_011.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductDetail
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string LanguageCode { get; set; }
        public string ProductName { get; set; }
        public string ProductInfor { get; set; }
    
        public virtual Category Category { get; set; }
        public virtual ProductAuction ProductAuction { get; set; }
    }
}
