﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EC04_011.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class AuctionSystemEntities : DbContext
    {
        public AuctionSystemEntities()
            : base("name=AuctionSystemEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<AuctionDetail> AuctionDetails { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<ProductAuction> ProductAuctions { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }
    }
}
